CREATE DATABASE tempbuddy;

\connect tempbuddy

CREATE TABLE "user" (id BIGSERIAL NOT NULL, email CHARACTER VARYING(30) NOT NULL, name CHARACTER VARYING(50), password CHARACTER VARYING(200), PRIMARY KEY (id), CONSTRAINT uk_email UNIQUE (email));

CREATE TABLE "server" (id BIGSERIAL NOT NULL, description CHARACTER VARYING(50), name CHARACTER VARYING(20), PRIMARY KEY (id));

CREATE TABLE "reserve" (id BIGSERIAL NOT NULL, end_date TIMESTAMP(6) WITHOUT TIME ZONE, start_date TIMESTAMP(6) WITHOUT TIME ZONE, server_id BIGINT, user_id BIGINT, PRIMARY KEY (id), CONSTRAINT fk3svosvwybf64758uvemp3c2wo FOREIGN KEY (server_id) REFERENCES "server" ("id"), CONSTRAINT fki6bn5amsqgmcae9clfv8y4oet FOREIGN KEY (user_id) REFERENCES "user" ("id"));

INSERT INTO public."user" (email, password, name) VALUES ('afuentes@example.com', '$2a$12$pO2jObz/0BwjCwiKDJryluigvXr9CXxtFgDtgycu/a3la3ISRlFiS', 'Alexis Fuentes');
INSERT INTO public."user" (email, password, name) VALUES ('example@example.com', '$2a$12$pO2jObz/0BwjCwiKDJryluigvXr9CXxtFgDtgycu/a3la3ISRlFiS', 'Example Example');

INSERT INTO public."server" (description, name) VALUES ('Development 1', 'dev1');
INSERT INTO public."server" (description, name) VALUES ('Development 2', 'dev2');
INSERT INTO public."server" (description, name) VALUES ('Development 3', 'dev3');
INSERT INTO public."server" (description, name) VALUES ('Development 4', 'dev4');
INSERT INTO public."server" (description, name) VALUES ('Development 5', 'dev5');
INSERT INTO public."server" (description, name) VALUES ('Development 6', 'dev6');

INSERT INTO public."reserve"(server_id, user_id, start_date, end_date) VALUES (1, 1, '2019-05-01 09:00:00', '2019-05-30 12:00:00');
INSERT INTO public."reserve"(server_id, user_id, start_date, end_date) VALUES (2, 2, '2019-05-01 10:00:00', '2019-05-12 08:00:00');
INSERT INTO public."reserve"(server_id, user_id, start_date, end_date) VALUES (1, 2, '2019-08-01 10:00:00', '2019-08-02 08:00:00');