INSERT INTO public.user (email, password, name) VALUES ('afuentes@example.com', '$2a$12$pO2jObz/0BwjCwiKDJryluigvXr9CXxtFgDtgycu/a3la3ISRlFiS', 'Alexis Fuentes');
INSERT INTO public.user (email, password, name) VALUES ('example@example.com', '$2a$12$pO2jObz/0BwjCwiKDJryluigvXr9CXxtFgDtgycu/a3la3ISRlFiS', 'Example Example');

INSERT INTO public.server (description, name) VALUES ('Development 1', 'dev1');
INSERT INTO public.server (description, name) VALUES ('Development 2', 'dev2');
INSERT INTO public.server (description, name) VALUES ('Development 3', 'dev3');
INSERT INTO public.server (description, name) VALUES ('Development 4', 'dev4');
INSERT INTO public.server (description, name) VALUES ('Development 5', 'dev5');
INSERT INTO public.server (description, name) VALUES ('Development 6', 'dev6');

INSERT INTO public.reserve(server_id, user_id, start_date, end_date) VALUES (1, 1, '2019-05-01 09:00:00', '2019-05-30 12:00:00');
INSERT INTO public.reserve(server_id, user_id, start_date, end_date) VALUES (2, 2, '2019-05-01 10:00:00', '2019-05-12 08:00:00');
INSERT INTO public.reserve(server_id, user_id, start_date, end_date) VALUES (1, 2, '2019-08-01 10:00:00', '2019-08-02 08:00:00');