package com.afuentes.tempbuddy.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="USER", uniqueConstraints={@UniqueConstraint(name="UK_EMAIL", columnNames={"EMAIL"})})
public class UserEntity {

	private static final int NAME_LENGTH = 50;
	private static final int PASSWORD_LENGTH = 200;
	private static final int EMAIL_LENGTH = 30;
	
	/**
	 * Identifier
	 */
	@Id
 	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	/**
	 * Email
	 */
	@Column(name="EMAIL", nullable=false, length=EMAIL_LENGTH)
	private String email;
	
	/**
	 * Password
	 */
	@Column(name="PASSWORD", length=PASSWORD_LENGTH)
	private String password;
	
	/**
	 * Name
	 */
	@Column(name="NAME", length=NAME_LENGTH)
	private String name;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
