package com.afuentes.tempbuddy.entities;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="RESERVE")
public class ReserveEntity {

	/**
	 * Identifier
	 */
	@Id
 	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	/**
	 * User
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="USER_ID")
	private UserEntity user;
	
	/**
	 * Server
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="SERVER_ID")
	private ServerEntity server;
	
	/**
	 * Start date
	 */
	@Column(name="START_DATE")
	private OffsetDateTime startDate;
	
	/**
	 * End date
	 */
	@Column(name="END_DATE")
	private OffsetDateTime endDate;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the user
	 */
	public UserEntity getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(UserEntity user) {
		this.user = user;
	}

	/**
	 * @return the server
	 */
	public ServerEntity getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(ServerEntity server) {
		this.server = server;
	}

	/**
	 * @return the startDate
	 */
	public OffsetDateTime getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(OffsetDateTime startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public OffsetDateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(OffsetDateTime endDate) {
		this.endDate = endDate;
	}
}
