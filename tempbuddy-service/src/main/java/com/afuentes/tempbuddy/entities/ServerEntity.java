package com.afuentes.tempbuddy.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="SERVER")
public class ServerEntity {

	private static final int NAME_LENGTH = 20;
	private static final int DESCRIPTION_LENGTH = 50;
	
	/**
	 * Identifier
	 */
	@Id
 	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	/**
	 * Name
	 */
	@Column(name="NAME", length=NAME_LENGTH)
	private String name;
	
	/**
	 * Description
	 */
	@Column(name="DESCRIPTION", length=DESCRIPTION_LENGTH)
	private String description;
	
	/**
	 * Reserves
	 */
	@OneToMany(mappedBy="server", fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ReserveEntity> reserves = new ArrayList<>();


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the reserves
	 */
	public List<ReserveEntity> getReserves() {
		return reserves;
	}


	/**
	 * @param reserves the reserves to set
	 */
	public void setReserves(List<ReserveEntity> reserves) {
		this.reserves = reserves;
	}
}
