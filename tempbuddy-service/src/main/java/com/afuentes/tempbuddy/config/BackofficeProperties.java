package com.afuentes.tempbuddy.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="backoffice")
public class BackofficeProperties {
	
	private String deployUrl;

	/**
	 * @return the deployUrl
	 */
	public String getDeployUrl() {
		return deployUrl;
	}

	/**
	 * @param deployUrl the deployUrl to set
	 */
	public void setDeployUrl(String deployUrl) {
		this.deployUrl = deployUrl;
	}
	

}
