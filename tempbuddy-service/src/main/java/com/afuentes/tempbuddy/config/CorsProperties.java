package com.afuentes.tempbuddy.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="cors")
public class CorsProperties {

	private Boolean enabled;	
	private String allowedOrigins;	
	private String allowedMethods;	
	private String pathPattern;

	/**
	 * @return the enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the allowedOrigins
	 */
	public String getAllowedOrigins() {
		return allowedOrigins;
	}

	/**
	 * @param allowedOrigins the allowedOrigins to set
	 */
	public void setAllowedOrigins(String allowedOrigins) {
		this.allowedOrigins = allowedOrigins;
	}

	/**
	 * @return the allowedMethods
	 */
	public String getAllowedMethods() {
		return allowedMethods;
	}

	/**
	 * @param allowedMethods the allowedMethods to set
	 */
	public void setAllowedMethods(String allowedMethods) {
		this.allowedMethods = allowedMethods;
	}

	/**
	 * @return the pathPattern
	 */
	public String getPathPattern() {
		return pathPattern;
	}

	/**
	 * @param pathPattern the pathPattern to set
	 */
	public void setPathPattern(String pathPattern) {
		this.pathPattern = pathPattern;
	}

	
}
