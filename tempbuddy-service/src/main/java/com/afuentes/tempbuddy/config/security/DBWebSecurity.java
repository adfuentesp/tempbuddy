package com.afuentes.tempbuddy.config.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.afuentes.tempbuddy.config.CorsProperties;
import com.afuentes.tempbuddy.controllers.BackofficeController;

@EnableGlobalMethodSecurity(prePostEnabled = true)
public class DBWebSecurity extends WebSecurityConfigurerAdapter {
		
	private static final String LOGIN_URL = "/login";
	private static final String LOGOUT_URL = "/logout";
	
	private static final String[] STATIC_MATCHERS = {
			"/assets/**",
	        "/css/**",
	        "/js/**",
	        "/img/**",
	        "*.ico"         	        	      
	};	
	
	private static final String[] APP_STATIC_MATCHERS = {
			BackofficeController.APP_PATH + BackofficeController.ASSETS,
			BackofficeController.APP_PATH + "/*.html",
			BackofficeController.APP_PATH + "/*.js",
			BackofficeController.APP_PATH + "/*.css",
			BackofficeController.APP_PATH + "/*.ico",
			BackofficeController.APP_PATH + "/*.txt",
			BackofficeController.APP_PATH + "/*.eot",
			BackofficeController.APP_PATH + "/*.svg",
			BackofficeController.APP_PATH + "/*.woff",
			BackofficeController.APP_PATH + "/*.woff2",
			BackofficeController.APP_PATH + "/*.ttf"
	};
	
	@Autowired
	private DBUserDetailsService userDetailsService;
	
	@Autowired
	private CorsProperties corsProperties;
			
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http		
			.csrf().disable()	
			.cors().configurationSource(corsConfigurationSource())
			.and()
			.authorizeRequests()
				.antMatchers(getRequestsAllowed()).permitAll()
				.anyRequest().authenticated()    			    			        
    			.and() 
			.formLogin()
	          .loginPage(LOGIN_URL)
	          .permitAll()
	          .and()
	        .logout()
        		.logoutUrl(LOGOUT_URL)
	        	.permitAll();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	    auth.authenticationProvider(authenticationProvider());
	}
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
	    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
	    authProvider.setUserDetailsService(userDetailsService);
	    authProvider.setPasswordEncoder(encoder());
	    return authProvider;
	}
	
	@Bean
	public PasswordEncoder encoder() {
	    return new CustomPasswordEncoder();
	}
	
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		final CorsConfiguration configuration = new CorsConfiguration();
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		if (corsProperties.getEnabled()) {
			configuration.setAllowedOrigins(Arrays.asList(corsProperties.getAllowedOrigins().split(",")));
			configuration.setAllowedMethods(Arrays.asList(corsProperties.getAllowedMethods().split(",")));
			configuration.addAllowedHeader("*");
			configuration.setAllowCredentials(true);

			source.registerCorsConfiguration(corsProperties.getPathPattern(), configuration);
		}
		return source;
	}
	
	private String[] getRequestsAllowed() {
    	List<String> allowed = new ArrayList<>(Arrays.asList(STATIC_MATCHERS));
    	allowed.addAll(Arrays.asList(APP_STATIC_MATCHERS));
    	return allowed.toArray(new String[0]);
    }

}
