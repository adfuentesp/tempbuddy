package com.afuentes.tempbuddy.config;

import java.io.IOException;
import java.time.OffsetDateTime;

import com.afuentes.tempbuddy.services.utils.DateUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class OffsetDateTimeLongSerializer extends JsonSerializer<OffsetDateTime> {

	@Override
	public void serialize(OffsetDateTime date, JsonGenerator jgen, SerializerProvider provider) throws IOException {
		jgen.writeNumber(DateUtils.getMilliseconds(date));
	}
}
