package com.afuentes.tempbuddy.config.security;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.afuentes.tempbuddy.entities.UserEntity;
import com.afuentes.tempbuddy.repositories.UserRepository;

@Service
@Transactional
public class DBUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepo;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// Find user
		UserEntity userEnt = userRepo.findByEmail(email);
		if (userEnt == null) {
			throw new UsernameNotFoundException(email + " not found!");
		}
		// Mapping data
		User user = new User();
		user.setEmail(userEnt.getEmail());
		user.setPassword(userEnt.getPassword());
		return new DBUserDetails(user);
	}

}
