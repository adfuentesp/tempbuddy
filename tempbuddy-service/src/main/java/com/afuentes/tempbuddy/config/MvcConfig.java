package com.afuentes.tempbuddy.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
	
	@Autowired
	private BackofficeProperties backofficeProperties;

	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addRedirectViewController("/", backofficeProperties.getDeployUrl());
		registry.addViewController("/login").setViewName("login");
	}
}
