package com.afuentes.tempbuddy.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="pagination")
public class PaginationProperties {
	
	private Integer defaultPageSize;	
	private Integer maxPageSize;
	
	/**
	 * @return the defaultPageSize
	 */
	public Integer getDefaultPageSize() {
		return defaultPageSize;
	}
	
	/**
	 * @param defaultPageSize the defaultPageSize to set
	 */
	public void setDefaultPageSize(Integer defaultPageSize) {
		this.defaultPageSize = defaultPageSize;
	}
	
	/**
	 * @return the maxPageSize
	 */
	public Integer getMaxPageSize() {
		return maxPageSize;
	}
	
	/**
	 * @param maxPageSize the maxPageSize to set
	 */
	public void setMaxPageSize(Integer maxPageSize) {
		this.maxPageSize = maxPageSize;
	}

}
