package com.afuentes.tempbuddy.config;

import java.io.IOException;
import java.time.OffsetDateTime;

import com.afuentes.tempbuddy.services.utils.DateUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class OffsetDateTimeLongDeserializer extends JsonDeserializer<OffsetDateTime> {

	@Override
	public OffsetDateTime deserialize(JsonParser parser, DeserializationContext context) throws IOException {
		String unixTimestamp = parser.getText().trim();
        return DateUtils.getOffsetDateTime(Long.valueOf(unixTimestamp));
	}
}
