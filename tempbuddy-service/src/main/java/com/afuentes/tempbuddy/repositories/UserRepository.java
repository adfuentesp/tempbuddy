package com.afuentes.tempbuddy.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.afuentes.tempbuddy.entities.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Serializable> {

	UserEntity findByEmail(String email);
	
}
