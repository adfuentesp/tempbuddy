package com.afuentes.tempbuddy.repositories;

import java.util.List;

import com.afuentes.tempbuddy.services.vo.ServerAbstract;

public interface ServerRepositoryCustom {

	List<ServerAbstract> findByCriteria(String name, Boolean available, Integer page, Integer pageSize);
	
	long countByCriteria(String name, Boolean available);
	
}
