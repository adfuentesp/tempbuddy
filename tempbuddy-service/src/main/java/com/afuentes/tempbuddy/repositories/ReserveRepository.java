package com.afuentes.tempbuddy.repositories;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.afuentes.tempbuddy.entities.ReserveEntity;
import com.afuentes.tempbuddy.entities.ServerEntity;

public interface ReserveRepository extends JpaRepository<ReserveEntity, Serializable> {

	@Query("SELECT r FROM ReserveEntity r WHERE r.server = :server "
			+ "AND ((:start >= r.startDate AND :start <= r.endDate) "
			+ "OR (:end >= r.startDate AND :end <= r.endDate))")
	List<ReserveEntity> findMatchDates(@Param("server") ServerEntity server, 
			@Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);
}
