package com.afuentes.tempbuddy.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.afuentes.tempbuddy.entities.ServerEntity;

public interface ServerRepository extends JpaRepository<ServerEntity, Serializable>, ServerRepositoryCustom {
	
}
