package com.afuentes.tempbuddy.repositories;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.afuentes.tempbuddy.services.vo.ServerAbstract;

public class ServerRepositoryImpl implements ServerRepositoryCustom {

	@Autowired
	private EntityManager em;
	
	@Override
	public List<ServerAbstract> findByCriteria(String name, Boolean available, Integer page, Integer pageSize) {
		// Create query
		StringBuilder sb = new StringBuilder();
		sb.append(getQueryFindByCriteria(name, available));
		// Limit and offset (pagination)
		sb.append("LIMIT :limit OFFSET :offset ");
		Query q = em.createNativeQuery(sb.toString());
		q.setParameter("limit", pageSize);
		q.setParameter("offset", (page-1)*pageSize);
		// Query params
		setParamsFindByCriteria(q, name, available);
		// Execute query
		@SuppressWarnings("unchecked")
		List<Object[]> results = q.getResultList();
		// Mapping results
		List<ServerAbstract> servers = new ArrayList<>();
		for(Object[] r : results) {
			ServerAbstract server = new ServerAbstract();
			server.setId(Long.valueOf(((BigInteger)r[0]).intValue()));
			server.setName((String)r[1]);
			server.setDescription((String)r[2]);
			server.setAvailable((Boolean)r[3]);
			servers.add(server);
		}
		return servers;
	}

	@Override
	public long countByCriteria(String name, Boolean available) {
		// Create query
		StringBuilder sbCount = new StringBuilder();
		sbCount.append("SELECT COUNT(*) FROM(");
		sbCount.append(getQueryFindByCriteria(name, available));
		sbCount.append(") A");
		Query qCount = em.createNativeQuery(sbCount.toString());
		// Query params
		setParamsFindByCriteria(qCount, name, available);
		// Execute query
		return ((BigInteger)qCount.getSingleResult()).longValue();
	}

	
	/**
	 * Query for FindByCriteria
	 * @param name
	 * @param available
	 * @return
	 */
	private String getQueryFindByCriteria(String name, Boolean available) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT A.id, A.name, A.description, A.available FROM (");
		sb.append("SELECT S.id, S.name, S.description, (R.id IS NULL) AS available ");
		sb.append("FROM {h-schema}server S ");
		sb.append("LEFT JOIN {h-schema}reserve R ON (S.id = R.server_id AND R.end_date > CURRENT_TIMESTAMP AND R.start_date < CURRENT_TIMESTAMP) ");
		if (!StringUtils.isEmpty(name)) {
			sb.append("WHERE S.name ILIKE :name ");
		}
		sb.append(") AS A ");
		if (available != null) {
			if (available) {
				sb.append("WHERE A.available IS TRUE ");
			} else {
				sb.append("WHERE A.available IS FALSE ");				
			}
		}
		sb.append("ORDER BY A.name ASC ");
		return sb.toString();
	}
		
	/**
	 * Set the params query for FindByCriteria
	 * @param name
	 * @param available
	 */
	private void setParamsFindByCriteria(Query q, String name, Boolean available) {
		if (!StringUtils.isEmpty(name)) {
			q.setParameter("name", "%" + name + "%");
		}
	}
}
