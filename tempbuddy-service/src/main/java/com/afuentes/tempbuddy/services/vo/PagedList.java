package com.afuentes.tempbuddy.services.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PagedList<T> implements Serializable {

	private static final long serialVersionUID = -7526158923875082369L;
	
	private long total = 0;	
	private int pageSize = 0;	
	private int page = 0;	
	private transient List<T> results = new ArrayList<>();

	/**
	 * @return the total
	 */
	public long getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(long total) {
		this.total = total;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the results
	 */
	public List<T> getResults() {
		return results;
	}

	/**
	 * @param results the results to set
	 */
	public void setResults(List<T> results) {
		this.results = results;
	}
}
