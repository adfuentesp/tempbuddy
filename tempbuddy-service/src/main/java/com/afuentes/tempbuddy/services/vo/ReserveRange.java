package com.afuentes.tempbuddy.services.vo;

import java.time.OffsetDateTime;

import com.afuentes.tempbuddy.config.OffsetDateTimeLongDeserializer;
import com.afuentes.tempbuddy.config.OffsetDateTimeLongSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ReserveRange {

	@JsonSerialize(using = OffsetDateTimeLongSerializer.class)
	@JsonDeserialize(using = OffsetDateTimeLongDeserializer.class)		
	private OffsetDateTime startDate;		
	@JsonSerialize(using = OffsetDateTimeLongSerializer.class)
	@JsonDeserialize(using = OffsetDateTimeLongDeserializer.class)
	private OffsetDateTime endDate;

	/**
	 * @return the startDate
	 */
	public OffsetDateTime getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(OffsetDateTime startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public OffsetDateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(OffsetDateTime endDate) {
		this.endDate = endDate;
	}
}
