package com.afuentes.tempbuddy.services.vo;

import java.io.Serializable;
import java.util.List;

public class ServerInformation implements Serializable {

	private static final long serialVersionUID = 1352943331157444085L;
	
	private Long id;		
	private String name;		
	private String description;			
	private List<Reserve> reservations;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the reservations
	 */
	public List<Reserve> getReservations() {
		return reservations;
	}

	/**
	 * @param reservations the reservations to set
	 */
	public void setReservations(List<Reserve> reservations) {
		this.reservations = reservations;
	}
}
