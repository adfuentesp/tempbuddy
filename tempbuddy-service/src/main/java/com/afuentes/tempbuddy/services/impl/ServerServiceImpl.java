package com.afuentes.tempbuddy.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.afuentes.tempbuddy.entities.ReserveEntity;
import com.afuentes.tempbuddy.entities.ServerEntity;
import com.afuentes.tempbuddy.exceptions.BaseException;
import com.afuentes.tempbuddy.exceptions.EntityNotFoundException;
import com.afuentes.tempbuddy.exceptions.InternalErrorException;
import com.afuentes.tempbuddy.repositories.ServerRepository;
import com.afuentes.tempbuddy.services.ServerService;
import com.afuentes.tempbuddy.services.utils.ParamsUtils;
import com.afuentes.tempbuddy.services.vo.PagedList;
import com.afuentes.tempbuddy.services.vo.Reserve;
import com.afuentes.tempbuddy.services.vo.ServerAbstract;
import com.afuentes.tempbuddy.services.vo.ServerCriteria;
import com.afuentes.tempbuddy.services.vo.ServerInformation;

@Service
public class ServerServiceImpl implements ServerService {

	@Autowired
	private ServerRepository serverRepo;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public PagedList<ServerAbstract> listServers(ServerCriteria criteria) {
		ParamsUtils.checkRequiredParam("criteria", criteria);
		ParamsUtils.checkRequiredParam("page", criteria.getPage());
		try {
			PagedList<ServerAbstract> servers = new PagedList<>();
			List<ServerAbstract> result = serverRepo.findByCriteria(criteria.getName(), criteria.getAvailable(), 
					criteria.getPage(), criteria.getPageSize());
			long total = serverRepo.countByCriteria(criteria.getName(), criteria.getAvailable());
			servers.setResults(result);
			servers.setTotal(total);
			servers.setPage(criteria.getPage());
			servers.setPageSize(criteria.getPageSize());
			return servers;
		} catch (InternalErrorException | IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException("An error has occurred listing the servers", e);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public ServerInformation getServer(Long id) throws EntityNotFoundException {
		ParamsUtils.checkRequiredParam("id", id);
		try {			
			Optional<ServerEntity> opt = serverRepo.findById(id);
			if (!opt.isPresent()) {
				throw new EntityNotFoundException("The server " + id + " does not exists");
			}
			// Mapping to VO
			return mapServer(opt.get());			
		} catch (BaseException | InternalErrorException | IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException("An error has occurred getting the server", e);
		}
	}
	
	@Override
	public ServerInformation createServer(ServerInformation serverInfo) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ServerInformation modifyServer(Long id, ServerInformation serverInfo) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteServer(Long id) {
		throw new UnsupportedOperationException();
	}
	

	/**
	 * Maps a ServerEntity to ServerInformation
	 * @param serverEnt
	 * @return
	 */
	private ServerInformation mapServer(ServerEntity serverEnt) {
		ServerInformation server = new ServerInformation();
		server.setId(serverEnt.getId());
		server.setName(serverEnt.getName());
		server.setDescription(serverEnt.getDescription());
		List<Reserve> reservations = new ArrayList<>();
		for(ReserveEntity reserveEnt : serverEnt.getReserves()) {
			Reserve reserve = new Reserve();
			reserve.setId(reserveEnt.getId());
			reserve.setStartDate(reserveEnt.getStartDate());
			reserve.setEndDate(reserveEnt.getEndDate());
			reserve.setEmail(reserveEnt.getUser().getEmail());
			reserve.setName(reserveEnt.getUser().getName());
			reservations.add(reserve);
		}
		server.setReservations(reservations);
		return server;
	}

}
