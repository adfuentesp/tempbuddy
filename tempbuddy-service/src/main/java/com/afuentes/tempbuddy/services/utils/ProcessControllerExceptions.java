package com.afuentes.tempbuddy.services.utils;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessControllerExceptions {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessControllerExceptions.class);
	
	private static final String ERROR_TITLE_HEADER = "errorTitle";
	private static final String ERROR_MSG_HEADER = "errorMessage";
	private static final String ERROR_CODE_HEADER = "errorCode";

	private ProcessControllerExceptions() {
		throw new IllegalStateException("Utility class!");
	}
	
	/**
	 * Send exception as JSON
	 * @param response
	 * @param errorTitle
	 * @param errorMsg
	 * @param status
	 */
	public static void sendException(HttpServletResponse response, String errorTitle, String errorMsg, Integer status) {
		try {						
			if (status == null) {
				status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
			}
			JSONObject respuesta = new JSONObject();
			respuesta.put(ERROR_CODE_HEADER, status);
			respuesta.put(ERROR_TITLE_HEADER, errorTitle);
			respuesta.put(ERROR_MSG_HEADER, errorMsg.replaceAll("\n","<br/>"));
			response.setContentType("application/json;charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(respuesta.toString());			
			response.setStatus(status);
			response.getWriter().flush();
		} catch (IOException e) {
			LOGGER.error("Error sending error", e);
		}		
	}
}
