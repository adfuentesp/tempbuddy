package com.afuentes.tempbuddy.services.utils;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

public class DateUtils {
		
	private DateUtils() {
		throw new IllegalStateException("Utility class!");
	}

	/**
	 * Get the timezone
	 * @return
	 */
	private static ZoneId timezone() {
		return TimeZone.getDefault().toZoneId();
	}
	
	/**
	 * Get OffsetDateTime of milliseconds
	 * @param milliseconds
	 * @return
	 */
	public static OffsetDateTime getOffsetDateTime(Long milliseconds) {
		try {
			return OffsetDateTime.ofInstant(Instant.ofEpochMilli(milliseconds), timezone());
		} catch (Exception e) {			
			throw new IllegalArgumentException("Fail to convert milliseconds to OffsetDateTime");
		}
	}
	
	/**
	 * Get milliseconds of OffsetDateTime
	 * @param offsetDateTime
	 * @return
	 */
	public static Long getMilliseconds(OffsetDateTime offsetDateTime) {
		if (offsetDateTime == null) {
			throw new IllegalArgumentException("OffsetDateTime missing");
		}
		return offsetDateTime.toEpochSecond() * 1000;
	}
}
