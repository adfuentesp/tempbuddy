package com.afuentes.tempbuddy.services;

import com.afuentes.tempbuddy.exceptions.EntityNotFoundException;
import com.afuentes.tempbuddy.services.vo.PagedList;
import com.afuentes.tempbuddy.services.vo.ServerAbstract;
import com.afuentes.tempbuddy.services.vo.ServerCriteria;
import com.afuentes.tempbuddy.services.vo.ServerInformation;

public interface ServerService {

	public PagedList<ServerAbstract> listServers(ServerCriteria criteria);
		
	public ServerInformation getServer(Long id) throws EntityNotFoundException;
	
	public ServerInformation createServer(ServerInformation serverInfo);
	
	public ServerInformation modifyServer(Long id, ServerInformation serverInfo);
	
	public void deleteServer(Long id);
	
}
