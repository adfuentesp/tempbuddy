package com.afuentes.tempbuddy.services.utils;

import java.text.MessageFormat;
import java.time.OffsetDateTime;

import org.springframework.util.StringUtils;

public class ParamsUtils {

	private ParamsUtils() {
		throw new IllegalStateException("Utility class!");
	}
	
	/**
	 * Check a required object
	 * @param paramName
	 * @param value
	 * @throws IllegalArgumentException
	 */
	public static void checkRequiredParam(String paramName, Object value) throws IllegalArgumentException {
		if (value == null) {
			throw new IllegalArgumentException(MessageFormat.format("The parameter [{0}] is required", paramName));
		}
	}
	
	/**
	 * Check a required string
	 * @param paramName
	 * @param value
	 * @throws IllegalArgumentException
	 */
	public static void checkRequiredString(String paramName, String value) throws IllegalArgumentException {
		if (StringUtils.isEmpty(value)) {
			throw new IllegalArgumentException(MessageFormat.format("The parameter [{0}] is required", paramName));
		}
	}
	
	/**
	 * Check data range
	 * @param startName
	 * @param start
	 * @param endName
	 * @param end
	 */
	public static void checkDateRange(String startName, OffsetDateTime start, String endName, OffsetDateTime end) {
		  if ((start != null) && (end != null)
				  && (end.compareTo(start) < 0)) {
			  throw new IllegalArgumentException("Date " + startName + " has to be lower than " + endName);
		  }
	  }
}
