package com.afuentes.tempbuddy.services.impl;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.afuentes.tempbuddy.entities.ReserveEntity;
import com.afuentes.tempbuddy.entities.ServerEntity;
import com.afuentes.tempbuddy.entities.UserEntity;
import com.afuentes.tempbuddy.exceptions.BaseException;
import com.afuentes.tempbuddy.exceptions.EntityNotFoundException;
import com.afuentes.tempbuddy.exceptions.InternalErrorException;
import com.afuentes.tempbuddy.repositories.ReserveRepository;
import com.afuentes.tempbuddy.repositories.ServerRepository;
import com.afuentes.tempbuddy.repositories.UserRepository;
import com.afuentes.tempbuddy.services.ReserveService;
import com.afuentes.tempbuddy.services.utils.ParamsUtils;
import com.afuentes.tempbuddy.services.vo.Reserve;
import com.afuentes.tempbuddy.services.vo.ServerInformation;

@Service
public class ReserveServiceImpl implements ReserveService {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private ReserveRepository reserveRepo;
	
	@Autowired
	private ServerRepository serverRepo;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ServerInformation reserveServer(Long serverId, OffsetDateTime startDate, OffsetDateTime endDate,
			String username) throws EntityNotFoundException {
		ParamsUtils.checkRequiredParam("serverId", serverId);
		ParamsUtils.checkRequiredParam("startDate", startDate);
		ParamsUtils.checkRequiredParam("endDate", endDate);
		ParamsUtils.checkRequiredString("username", username);
		ParamsUtils.checkDateRange("startDate", startDate, "endDate", endDate);
		try {
			// Retrieve user
			UserEntity userEnt = getUser(username);
			// Retrieve server
			ServerEntity serverEnt = getServer(serverId);
			// Check if the reservation does not match with any
			List<ReserveEntity> reservations = reserveRepo.findMatchDates(serverEnt, startDate, endDate);
			if ((reservations != null) && (!reservations.isEmpty())) {
				throw new IllegalArgumentException("The dates matches with existing reservations");
			}
			// Create reservation
			ReserveEntity reserveEnt = new ReserveEntity();
			reserveEnt.setStartDate(startDate);
			reserveEnt.setEndDate(endDate);
			reserveEnt.setServer(serverEnt);
			reserveEnt.setUser(userEnt);
			reserveRepo.save(reserveEnt);
			return mapServer(serverEnt);
		} catch (BaseException | InternalErrorException | IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException("An error has occurred reserving the server", e);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ServerInformation cancelServerReservation(Long serverId, Long reserveId, String email) {
		ParamsUtils.checkRequiredParam("serverId", serverId);
		ParamsUtils.checkRequiredParam("reserveId", reserveId);
		ParamsUtils.checkRequiredString("email", email);
		try {
			// Get the reservation
			ReserveEntity reserveEnt = getReserve(reserveId);
			// Checks that it belongs to server
			if (!serverId.equals(reserveEnt.getServer().getId())) {
				throw new IllegalArgumentException("The reservation " + reserveId + " does not belong to the server " + serverId);
			}
			// Checks that it belongs to the user
			if (!reserveEnt.getUser().getEmail().equals(email)) {
				throw new IllegalArgumentException("The reservation " + reserveId + " does not belong to the user " + email);
			}
			// Cancel reservation
			reserveRepo.delete(reserveEnt);			
			reserveRepo.flush();
			return mapServer(getServer(serverId));
		} catch (InternalErrorException | IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException("An error has occurred canceling the reservation", e);
		}
	}
	
	/**
	 * Get a user by its email
	 * @param username
	 * @return
	 * @throws EntityNotFoundException
	 */
	private UserEntity getUser(String email) throws EntityNotFoundException {
		UserEntity userEnt = userRepo.findByEmail(email);
		if (userEnt == null) {
			throw new EntityNotFoundException("The user " + email + " does not exist");
		}
		return userEnt;
	}
	
	/**
	 * Get a server
	 * @param serverId
	 * @return
	 * @throws EntityNotFoundException
	 */
	private ServerEntity getServer(Long serverId) throws EntityNotFoundException {
		Optional<ServerEntity> opt = serverRepo.findById(serverId);
		if (!opt.isPresent()) {
			throw new EntityNotFoundException("The server " + serverId + " does not exists");
		}
		return opt.get();
	}
	
	/**
	 * Get a reservation
	 * @param reserveId
	 * @return
	 * @throws EntityNotFoundException 
	 */
	private ReserveEntity getReserve(Long reserveId) throws EntityNotFoundException {
		Optional<ReserveEntity> opt = reserveRepo.findById(reserveId);
		if (!opt.isPresent()) {
			throw new EntityNotFoundException("The reserve " + reserveId + " does not exists");
		}
		return opt.get();
	}
	
	/**
	 * Maps a ServerEntity to ServerInformation
	 * @param serverEnt
	 * @return
	 */
	private ServerInformation mapServer(ServerEntity serverEnt) {
		ServerInformation server = new ServerInformation();
		server.setId(serverEnt.getId());
		server.setName(serverEnt.getName());
		server.setDescription(serverEnt.getDescription());
		List<Reserve> reservations = new ArrayList<>();
		for(ReserveEntity reserveEnt : serverEnt.getReserves()) {
			Reserve reserve = new Reserve();
			reserve.setId(reserveEnt.getId());
			reserve.setStartDate(reserveEnt.getStartDate());
			reserve.setEndDate(reserveEnt.getEndDate());
			reserve.setEmail(reserveEnt.getUser().getEmail());
			reserve.setName(reserveEnt.getUser().getName());
			reservations.add(reserve);
		}
		server.setReservations(reservations);
		return server;
	}

}
