package com.afuentes.tempbuddy.services.vo;

import java.io.Serializable;
import java.time.OffsetDateTime;

import com.afuentes.tempbuddy.config.OffsetDateTimeLongDeserializer;
import com.afuentes.tempbuddy.config.OffsetDateTimeLongSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Reserve implements Serializable {

	private static final long serialVersionUID = 5283382289660766524L;
	
	private Long id;
	private String email;
	private String name;
	@JsonSerialize(using = OffsetDateTimeLongSerializer.class)
	@JsonDeserialize(using = OffsetDateTimeLongDeserializer.class)
	private OffsetDateTime startDate;	
	@JsonSerialize(using = OffsetDateTimeLongSerializer.class)
	@JsonDeserialize(using = OffsetDateTimeLongDeserializer.class)
	private OffsetDateTime endDate;
	
	
	/**
	 * @return the startDate
	 */
	public OffsetDateTime getStartDate() {
		return startDate;
	}
	
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(OffsetDateTime startDate) {
		this.startDate = startDate;
	}
	
	/**
	 * @return the endDate
	 */
	public OffsetDateTime getEndDate() {
		return endDate;
	}
	
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(OffsetDateTime endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
