package com.afuentes.tempbuddy.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afuentes.tempbuddy.entities.UserEntity;
import com.afuentes.tempbuddy.exceptions.EntityNotFoundException;
import com.afuentes.tempbuddy.repositories.UserRepository;
import com.afuentes.tempbuddy.services.UserService;
import com.afuentes.tempbuddy.services.vo.UserSimple;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepo;

	@Override
	public UserSimple getUser(String email) throws EntityNotFoundException {
		UserEntity userEnt = userRepo.findByEmail(email);
		if (userEnt == null) {
			throw new EntityNotFoundException("User " + email + " not found");
		}
		// Mapping
		UserSimple user = new UserSimple();
		user.setId(userEnt.getId());
		user.setEmail(userEnt.getEmail());
		user.setName(userEnt.getName());
		return user;
	}
	
}
