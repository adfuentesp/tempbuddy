package com.afuentes.tempbuddy.services;

import java.time.OffsetDateTime;

import com.afuentes.tempbuddy.exceptions.EntityNotFoundException;
import com.afuentes.tempbuddy.services.vo.ServerInformation;

public interface ReserveService {

	public ServerInformation reserveServer(Long serverId, OffsetDateTime startDate, OffsetDateTime endDate, String username) throws EntityNotFoundException;
	
	public ServerInformation cancelServerReservation(Long serverId, Long reserveId, String username);
	
}
