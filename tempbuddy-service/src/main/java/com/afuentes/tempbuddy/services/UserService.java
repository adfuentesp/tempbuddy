package com.afuentes.tempbuddy.services;

import com.afuentes.tempbuddy.exceptions.EntityNotFoundException;
import com.afuentes.tempbuddy.services.vo.UserSimple;

public interface UserService {
	
	public UserSimple getUser(String email) throws EntityNotFoundException;

}
