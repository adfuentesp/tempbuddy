package com.afuentes.tempbuddy.exceptions;

public abstract class BaseException extends Exception {

	private static final long serialVersionUID = -7570511050831135993L;

	public BaseException(String message, Throwable cause) {
		super(message, cause);
	}

	public BaseException(String message) {
		super(message);
	}

	public BaseException(Throwable cause) {
		super(cause);
	}
}
