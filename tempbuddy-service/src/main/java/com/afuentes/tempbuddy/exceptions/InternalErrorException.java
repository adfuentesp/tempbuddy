package com.afuentes.tempbuddy.exceptions;

public class InternalErrorException extends RuntimeException {

	private static final long serialVersionUID = -3491551254942794299L;

	public InternalErrorException(String message) {
		super(message);
	}

	public InternalErrorException(String message, Throwable cause) {
		super(message, cause);
	}

	public InternalErrorException(Throwable cause) {
		super(cause);
	}
}
