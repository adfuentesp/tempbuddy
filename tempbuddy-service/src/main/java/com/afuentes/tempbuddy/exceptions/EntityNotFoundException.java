package com.afuentes.tempbuddy.exceptions;

public class EntityNotFoundException extends BaseException {
	
	private static final long serialVersionUID = 3309053633559962624L;

	public EntityNotFoundException(String message) {
		super(message);
	}

	public EntityNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public EntityNotFoundException(Throwable cause) {
		super(cause);
	}
}
