package com.afuentes.tempbuddy.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afuentes.tempbuddy.config.PaginationProperties;
import com.afuentes.tempbuddy.config.ServiceConfig;
import com.afuentes.tempbuddy.services.ServerService;
import com.afuentes.tempbuddy.services.utils.ProcessControllerExceptions;
import com.afuentes.tempbuddy.services.vo.PagedList;
import com.afuentes.tempbuddy.services.vo.ServerAbstract;
import com.afuentes.tempbuddy.services.vo.ServerCriteria;
import com.afuentes.tempbuddy.services.vo.ServerInformation;

@RestController
@RequestMapping(ServiceConfig.SERVICE_API + "server")
public class ServerController {

	@Autowired
	private ServerService serverService;
	
	@Autowired
	private PaginationProperties paginationProperties;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public PagedList<ServerAbstract> listServers(
			@RequestParam(required=false) String name,
			@RequestParam(required=false) Boolean available,
			@RequestParam(required=false) Integer page,
			@RequestParam(required=false) Integer pageSize,
			HttpServletResponse response,
			HttpServletRequest request,
			Authentication auth) {		
		try {
			// Pagination control
			if (page == null || page < 1) {
				page = 1;
			}
			if (pageSize == null || pageSize <= 0) {
				pageSize = paginationProperties.getDefaultPageSize();
			} else if (pageSize > paginationProperties.getMaxPageSize()){
				pageSize = paginationProperties.getMaxPageSize();
			}
			// Criteria
			ServerCriteria criteria = new ServerCriteria();
			criteria.setName(name);
			criteria.setAvailable(available);			
			criteria.setPage(page);
			criteria.setPageSize(pageSize);
			return serverService.listServers(criteria);
		} catch (Throwable t) {
			ProcessControllerExceptions.sendException(response, "Error listing servers", t.getMessage(), null);
			return null;
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ServerInformation getServer(
			@PathVariable Long id,
			HttpServletResponse response,
			HttpServletRequest request,
			Authentication auth) {
		try {
			return serverService.getServer(id);
		} catch (Throwable t) {
			ProcessControllerExceptions.sendException(response, "Error getting a server", t.getMessage(), null);
			return null;
		}
	}
	
}
