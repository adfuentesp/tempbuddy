package com.afuentes.tempbuddy.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorHandlerController implements ErrorController {

	private static final String PATH = "/error/";
	
	private static final String FORBIDDEN = "403.html";
	private static final String NOT_FOUND = "404.html";
	private static final String NOT_ALLOWED = "405.html";
	private static final String DEFAULT = "error.html";
	
	@GetMapping("/error")
    public String errorHandler(HttpServletRequest request, HttpServletResponse response) {
		switch(response.getStatus()) {
			case 403: return PATH + FORBIDDEN;
			case 404: String referer = (String) request.getAttribute("javax.servlet.forward.request_uri");
					  if ((referer != null) && (referer.matches(".*" + BackofficeController.APP_PATH + "/.*"))) {
						  response.setStatus(HttpServletResponse.SC_OK);
						  return BackofficeController.APP_INDEX;
					  }
					  return PATH + NOT_FOUND; 
			case 405: return PATH + NOT_ALLOWED; 
			default	: return PATH + DEFAULT;
		}
    }
	
	@Override
	public String getErrorPath() {
		return "/error";
	}

}
