package com.afuentes.tempbuddy.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BackofficeController {

	public static final String APP_PATH = "/app";
	public static final String APP_INDEX = APP_PATH + "/index.html";
	public static final String ASSETS = "/assets/**";
	
	@RequestMapping(APP_PATH)
	String app() {
		return APP_INDEX;
	}
	
	@RequestMapping(ASSETS)
	String assets(HttpServletRequest request) {
		return "redirect:" + APP_PATH + request.getServletPath();
	}
}
