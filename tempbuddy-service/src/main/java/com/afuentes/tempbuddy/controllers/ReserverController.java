package com.afuentes.tempbuddy.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.afuentes.tempbuddy.config.ServiceConfig;
import com.afuentes.tempbuddy.services.ReserveService;
import com.afuentes.tempbuddy.services.utils.ProcessControllerExceptions;
import com.afuentes.tempbuddy.services.vo.Reserve;
import com.afuentes.tempbuddy.services.vo.ReserveRange;
import com.afuentes.tempbuddy.services.vo.ServerInformation;

@RestController
@RequestMapping(ServiceConfig.SERVICE_API + "server")
public class ReserverController {

	@Autowired
	private ReserveService reserveService;
	
	@RequestMapping(value = "/{serverId}/reserve", method = RequestMethod.POST)
	public ServerInformation reserveServer(
			@PathVariable("serverId") Long serverId,
			@RequestBody ReserveRange reserve,
			HttpServletResponse response,
			HttpServletRequest request, 
			Authentication auth) {
		try {
			return reserveService.reserveServer(serverId, reserve.getStartDate(), reserve.getEndDate(), auth.getName());
		} catch (Throwable t) {
			ProcessControllerExceptions.sendException(response, "Error reserving a server", t.getMessage(), null);
			return null;
		}
	}
	
	@RequestMapping(value = "/{serverId}/reserve/{id}", method = RequestMethod.DELETE)
	public ServerInformation cancelServerReservation(
			@PathVariable("serverId") Long serverId,
			@PathVariable("id") Long id,
			HttpServletResponse response,
			HttpServletRequest request, 
			Authentication auth) {
		try {
			return reserveService.cancelServerReservation(serverId, id, auth.getName());
		} catch (Throwable t) {
			ProcessControllerExceptions.sendException(response, "Error canceling a server reservation", t.getMessage(), null);
			return null;
		}
	}
}
