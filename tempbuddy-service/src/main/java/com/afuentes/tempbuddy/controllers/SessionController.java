package com.afuentes.tempbuddy.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.afuentes.tempbuddy.config.ServiceConfig;
import com.afuentes.tempbuddy.services.UserService;
import com.afuentes.tempbuddy.services.utils.ProcessControllerExceptions;
import com.afuentes.tempbuddy.services.vo.UserSimple;

@RestController
@RequestMapping(ServiceConfig.SERVICE_API + "session")
public class SessionController {

	@Autowired
	private UserService userService;
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public UserSimple getLoggedUser(			 
			HttpServletRequest request, 
			HttpServletResponse response,
			Authentication auth) {
		try {
			return userService.getUser(auth.getName());
		} catch (Throwable t) {
			ProcessControllerExceptions.sendException(response, "Error getting user logged in", t.getMessage(), null);
			return null;
		}
	}
	
}
