package com.afuentes.tempbuddy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TempbuddyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TempbuddyApplication.class, args);
	}

}
