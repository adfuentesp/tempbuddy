# Run docker

Go to **/tempbuddy-docker** and run **docker-compose up**.

If you want to compile the app, follow the next steps and replace the result at **/tempbuddy-docker/app/tempbuddy-package-0.0.1.jar**.

If you want to change properties, go to **/tempbuddy-docker/app/application.properties**.  It is prepared to work with the configuration of the Docker, but if you need to change the ports or IP where they are published, it will be necessary to change the connection URL to the database of the application. (_spring.datasource.url_).

# Compile app

Go to /**tempbuddy-package** and run **mvn clean package**.

The result will be at /tempbuddy-package/target/**tempbuddy-package-0.0.1.jar**


# Access to app

http://192.168.99.100:8080  (Docker default IP)

User: afuentes@example.com
Password: 12345678

User: example@example.com
Password: 12345678