export const environment = {
  production: true,
  api: (<any>window)._environment.api,
  login: (<any>window)._environment.login,
  logout: (<any>window)._environment.logout
};
