import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, Injectable } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GlobalsService } from './globals.service';
import { HttpClientModule, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ServersComponent } from './servers/servers.component';
import { FormsModule } from '@angular/forms';
import { ServerComponent } from './servers/server/server/server.component';
import { ReserveComponent } from './servers/server/server/reserve/reserve.component';

/**
 * Interceptor for set credentials on request
 */
@Injectable()
export class GlobalHttpInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      withCredentials: true
    });
    return next.handle(req).pipe(
      tap((event: HttpEvent<any>) => null, (error: any) => {})
    );
  }
}

/**
 * Factory to initialize app
 */
export function initFactory(provider: GlobalsService) {
  return () => {
    return provider.setLoggedUser();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    ServersComponent,
    ServerComponent,
    ReserveComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule
  ],
  entryComponents: [
    ReserveComponent
  ],
  providers: [
    GlobalsService,
    {
      provide: APP_INITIALIZER, useFactory: initFactory, deps: [GlobalsService], multi: true
    },
    {
      provide: HTTP_INTERCEPTORS, useClass: GlobalHttpInterceptor, multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
