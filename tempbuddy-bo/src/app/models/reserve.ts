export class Reserve {
    id: number;
    email: string;
    name: string;
    startDate: number;
    endDate: number;
}
