import { Reserve } from './reserve';

export class Server {
    id: number;
    name: string;
    description: string;
    reservations: Reserve[];

    constructor() {
        this.reservations = [];
    }
}
