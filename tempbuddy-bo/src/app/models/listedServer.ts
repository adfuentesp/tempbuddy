export class ListedServer {
    id: number;
    name: string;
    description: string;
    available: boolean;
}
