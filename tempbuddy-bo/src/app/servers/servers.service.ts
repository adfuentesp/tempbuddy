import { Injectable } from '@angular/core';
import { GlobalsService } from '../globals.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServersService {

  constructor(
    private http: HttpClient,
    private globals: GlobalsService
  ) { }

  listServers(filter) {
    let cleanFilter = {} as any;
    cleanFilter = Object.keys(filter).filter(key => {
      return filter[key] !== null && filter[key] !== undefined && filter[key] !== '';
    }).reduce((obj, key) => {
      obj[key] = filter[key];
      return obj;
    }, {});
    return this.http.get(`${this.globals.API}/server`, {params: cleanFilter});
  }

  getServer(id: number) {
    return this.http.get(`${this.globals.API}/server/${id}`, {});
  }

  createReservation(serverId: number, reservation) {
    return this.http.post(`${this.globals.API}/server/${serverId}/reserve`, reservation);
  }

  deleteReservartion(serverId: number, reserveId: number) {
    return this.http.delete(`${this.globals.API}/server/${serverId}/reserve/${reserveId}`);
  }
}
