import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ReserveRange } from 'src/app/models/reserveRange';
import { Subscription } from 'rxjs';
import { ServersService } from 'src/app/servers/servers.service';

@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.css']
})
export class ReserveComponent implements OnInit {

  promiseList: Subscription;
  subs: Subscription = new Subscription();

  @Input() serverId: number;
  @Input() reserveRange: ReserveRange;

  startDateRaw: NgbDateStruct;
  endDateRaw: NgbDateStruct;

  constructor(
    private activeModal: NgbActiveModal,
    private serverService: ServersService
  ) { }

  ngOnInit() {}

  save() {
    this.reserveRange.startDate = this.getDateFromRaw(this.startDateRaw);
    this.reserveRange.endDate = this.getDateFromRaw(this.endDateRaw);
    this.subs.add(this.promiseList = this.serverService.createReservation(this.serverId, this.reserveRange).subscribe(
      () => this.activeModal.close(),
      error => alert(error.error.errorMessage)
    ));
  }

  cancel() {
    this.activeModal.dismiss();
  }

  private getDateFromRaw(dateRaw: NgbDateStruct) {
    if (dateRaw) {
      return new Date(dateRaw.year, dateRaw.month - 1, dateRaw.day).getTime();
    } else {
      return undefined;
    }
  }

}
