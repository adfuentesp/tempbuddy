import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Server } from 'src/app/models/server';
import { ActivatedRoute, Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ServersService } from '../../servers.service';
import { Reserve } from 'src/app/models/reserve';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReserveComponent } from './reserve/reserve.component';
import { ReserveRange } from 'src/app/models/reserveRange';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {

  promiseList: Subscription;
  subs: Subscription = new Subscription();

  server: Server = new Server();
  id: number;
  name: string;
  description: string;
  reservations: Reserve[];
  totalReservations: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private serversService: ServersService,
    private modals: NgbModal,
    public globals: GlobalsService,
  ) { }

  ngOnInit() {
    this.subs.add(this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getServer();
    }));
  }

  private getServer() {
    this.subs.add(this.promiseList = this.serversService.getServer(this.id).subscribe((server: Server) => {
      this.server.reservations = [];
      this.server = server;
      this.name = server.name;
      this.description = server.description;
      this.reservations = server.reservations;
      this.totalReservations = this.reservations.length;
    }, error => alert(error.error.errorMessage)));
  }

  /**
   * Open a modal for reservation
   */
  openReservation() {
    const modalRef = this.modals.open(ReserveComponent, {backdrop: 'static'});
    modalRef.componentInstance.serverId = this.server.id;
    modalRef.componentInstance.reserveRange = new ReserveRange();
    modalRef.result.then(
      () => this.getServer(),
      () => null
    );
  }

  /**
   * Cancel a reservation
   */
  cancelReservation(reserveId: number) {
    this.subs.add(this.promiseList = this.serversService.deleteReservartion(this.id, reserveId).subscribe(
      () => this.getServer(),
      error => alert(error.error.errorMessage)
    ));
  }
}
