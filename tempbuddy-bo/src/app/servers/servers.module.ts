import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServersService } from './servers.service';
import { FormsModule } from '@angular/forms';
import { ServerComponent } from './server/server/server.component';
import { ReserveComponent } from './server/server/reserve/reserve.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule
  ],
  providers: [
    ServersService
  ],
  entryComponents: [
    ReserveComponent
  ]
})
export class ServersModule { }
