import { Component, OnInit } from '@angular/core';
import { ServersService } from './servers.service';
import { GlobalsService } from '../globals.service';
import { ListedServer } from '../models/listedServer';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  promiseList: Subscription;
  subs: Subscription = new Subscription();

  servers: ListedServer[] = [];
  totalItems: number;

  filterOptions: any = {
    name: '',
    available: '',
    page: 1,
    pageSize: 10
  };

  constructor(
    private route: ActivatedRoute,
    private serversService: ServersService,
    public globals: GlobalsService,
    private router: Router
    ) { }

  ngOnInit() {
    this.loadParameters().then(() => this.findServers());
  }

  private loadParameters() {
    return new Promise((resolve) => {
      this.route.queryParams.subscribe((params) => {
        if (params.name) {
          this.filterOptions.name = params.name;
        }
        if (params.available) {
          this.filterOptions.available = params.available;
        }
        if (params.page) {
          this.filterOptions.page = params.page;
        }
        if (params.pageSize) {
          this.filterOptions.pageSize = params.pageSize;
        }
        resolve();
      });
    });
  }

  /**
   * Find servers
   */
  private findServers() {
    this.subs.add(this.promiseList = this.serversService.listServers(this.filterOptions).subscribe((data: any) => {
      this.servers = data.results;
      this.totalItems = data.total;
    }, error => alert(error.error.errorMessage)));
  }

  /**
   * Open a server
   */
  openServer(server: ListedServer) {
    this.router.navigateByUrl(`servers/${server.id}`);
  }

  /**
   * Execute find from button or paginator
   */
  executeFind(paginator: boolean) {
    this.router.navigate(['servers'], {
      queryParams: {
        name: this.filterOptions.name,
        available: this.filterOptions.available,
        page: paginator ? this.filterOptions.page : 1,
        pageSize: this.filterOptions.pageSize
      }
    }).then(() => this.findServers());
  }
}
