import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GlobalsService {

  loggedUser: any;

  LOGIN = environment.login;
  LOGOUT = environment.logout;
  API = environment.api;

  constructor(private http: HttpClient) { }

  setLoggedUser() {
    return new Promise((resolve) => {
      this.http.get(environment.api + '/session').subscribe(result => {
        this.loggedUser = result;
        resolve(true);
      }, () => {
        alert('Error getting user information...');
        location.href = environment.login;
        resolve();
      });
    });
  }
}
